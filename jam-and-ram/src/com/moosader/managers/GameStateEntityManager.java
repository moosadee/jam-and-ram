package com.moosader.managers;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.moosader.entities.Boss;
import com.moosader.entities.Bullet;
import com.moosader.entities.Enemy;
import com.moosader.entities.EntityBase;
import com.moosader.entities.Explosion;
import com.moosader.entities.Player;
import com.moosader.entities.Player.GunnerState;
import com.moosader.entities.Player.PilotState;
import com.moosader.screens.GameScreen;

public class GameStateEntityManager {
	private Vector2 m_screenMaxCoords;
	
	private ArrayList<Enemy> m_lstEnemies;
	private ArrayList<Bullet> m_lstBullets;
	private ArrayList<Explosion> m_lstExplosions;
	private Player m_player;
	private Boss m_boss;
	
	private float m_timeSinceLastSpawn;
	private float m_timeBetweenSpawns = 100.0f;
	private Random m_randGenerator;
	
	private enum GameState {
		ACTIVE, PLAYER_DEAD, BOSS_DEAD
	};
	private GameState m_gameState;
	private int m_gameOverCounter;
	
	public Player getPlayer() {
		return m_player;
	}
	
	public void setup(int level) {
		m_gameState = GameState.ACTIVE;
		m_screenMaxCoords = new Vector2(1000, 1000);
		m_randGenerator = new Random(124);
		m_timeSinceLastSpawn = 0.0f;
		
		m_player = new Player();
		m_player.setup();
		m_boss = new Boss();
		m_boss.setup();
		m_lstBullets = new ArrayList<Bullet>();
		m_lstEnemies = new ArrayList<Enemy>();
		m_lstExplosions = new ArrayList<Explosion>();
		m_gameOverCounter = 0;
		
		if ( level == 1 ) {
			m_timeBetweenSpawns = 100.0f;		
		}
		else {
			m_timeBetweenSpawns = 50.0f;
			m_boss.setMaxHP(70 * 30);
		}
	}
	
	public boolean nextLevel() {
		return ( m_gameOverCounter >= 200 && m_gameState == GameState.BOSS_DEAD );		
	}
	
	public boolean gameOver() {
		return ( m_gameOverCounter >= 200 && m_gameState == GameState.PLAYER_DEAD ); 
	}
	
	public void update(float delta) {
		updateBullets(delta);
		updateEnemies(delta);
		if (m_gameState == GameState.PLAYER_DEAD) {
			m_gameOverCounter++;
			if ( m_player.crash() && m_gameOverCounter % 5 == 0 ) {
				// Keep generating explosions on player
				Random rand = new Random();
				Explosion newExplosion = new Explosion(m_player);
				newExplosion.setCoordinates( new Vector2( rand.nextInt( (int) m_player.getWidth() ) + m_player.getLeft(),
						rand.nextInt( (int) m_player.getHeight() ) + m_player.getBottom() - 64 ) );
				newExplosion.setSize(rand.nextInt(64) + 64);
				m_lstExplosions.add(new Explosion(newExplosion));				
			}
		}
		else if (m_gameState == GameState.BOSS_DEAD) {
			m_gameOverCounter++;
			System.out.println("Count: " + m_gameOverCounter );
			if ( m_boss.crash() && m_gameOverCounter % 5 == 0 ) {
				// Keep generating explosions on player
				Random rand = new Random();
				Explosion newExplosion = new Explosion(m_boss);
				newExplosion.setCoordinates( new Vector2( rand.nextInt( (int) m_boss.getWidth() ) + m_boss.getLeft(),
						rand.nextInt( (int) m_boss.getHeight() ) + m_boss.getBottom() - 64 ) );
				newExplosion.setSize(rand.nextInt(64) + 64);
				m_lstExplosions.add(new Explosion(newExplosion));				
			}			
		}
		else {
			updatePlayer(delta);			
		}
		updateEffects(delta);
	}
	
	public void setPilotHuman(boolean isHuman) {
		if (isHuman) {
			m_player.m_pilotControl = PilotState.PLAYER;
		}
		else {
			m_player.m_pilotControl = PilotState.AI;
		}
	}
	
	public void setGunnerHuman(boolean isHuman) {
		if (isHuman) {
			m_player.m_gunnerControl = GunnerState.PLAYER;
		}
		else {
			m_player.m_gunnerControl = GunnerState.AI;
		}		
	}
	
	public boolean isPilotHuman() {
		return (m_player.m_pilotControl == PilotState.PLAYER) ? true : false;
	}
	
	public boolean isGunnerHuman() {
		return (m_player.m_gunnerControl == GunnerState.PLAYER) ? true : false;		
	}
	
	public void addEntitiesToSpriteList(GameScreen screen) {
		m_player.addToSpriteList(screen);
		m_boss.addToSpriteList(screen);
		//m_player.addToSpriteList(lstSprites);
		for (Enemy e : m_lstEnemies) {
			e.addToSpriteList(screen);
		}
		for (Bullet b : m_lstBullets)
		{
			screen.add(b.m_sprite);
		}		
		for(Explosion e : m_lstExplosions) {
			screen.add(e.m_sprite);
		}
	}
	
	private void updatePlayer(float delta) {
		m_player.handleMovement(m_boss);
		m_player.update(delta);
		
		// Check for shot fired from player
		if (m_player.releaseBullet()) {
			m_lstBullets.add(new Bullet(m_player.getChargingBullet()));
		}
	}
	
	private void updateEffects(float delta) {
		ArrayList<Explosion> explosionsToRemove = new ArrayList<Explosion>();
		
		for(Explosion e : m_lstExplosions) {
			e.update(delta);
			if (e.isActive() == false) {
				explosionsToRemove.add(e);
			}
		}
		
		m_lstExplosions.removeAll(explosionsToRemove);
	}
	
	private void updateBullets(float delta) {
		m_boss.handleMovement();
		m_boss.update(delta);
		// Update all bullets	
		ArrayList<Bullet> bulletsToRemove = new ArrayList<Bullet>();
		ArrayList<Enemy> enemiesToRemove = new ArrayList<Enemy>();
		for (Bullet b : m_lstBullets)
		{
			b.move(delta);
			if (b.getLeft() > 1000) {
				// Kill this bullet
				bulletsToRemove.add(b);
			}
			else if (b.getRight() < 0 ) {
				bulletsToRemove.add(b);
			}
			// Check collision between bullet and enemies
			// TODO: Clean
			// Enemy bullets don't hurt themselves
			if (b.isPlayerBullet()) {
				for(Enemy e: m_lstEnemies) {
					if (isCollision(b, e)) {
						// Hurt enemy
						e.hitByBullet(b.getSize());
						bulletsToRemove.add(b);
						
						if ( e.getHP() <= 0 ) {
							enemiesToRemove.add(e);
							// Add explosion
							m_lstExplosions.add(new Explosion(b, 128f));
						}
						else {
							// Add explosion
							m_lstExplosions.add(new Explosion(b));						
						}
					}
				}
				if (isCollision(b, m_boss)) {
					m_boss.hitByBullet(b.getSize());
					bulletsToRemove.add(b);
					m_lstExplosions.add(new Explosion(b));		
				}
				
				if (m_boss.getHP() <= 0 ) {
					m_gameState = GameState.BOSS_DEAD;
				}
			}
			else {
				if (m_boss.getHP() > 0 && isCollision(b, m_player)) {
					// Add explosion
					m_lstExplosions.add(new Explosion(b));
					// Hurt player
					m_player.hitByBullet(b.getSize());
					bulletsToRemove.add(b);
				}
				
				if (m_player.getHP() <= 0 ) {
					m_gameState = GameState.PLAYER_DEAD;
				}
			}
		}
		
		for (Bullet b : bulletsToRemove) {
			m_lstBullets.remove(b);
		}
		for (Enemy e : enemiesToRemove) {
			m_lstEnemies.remove(e);
		}
	}
	
	private void updateEnemies(float delta) {
		// m_enemies
		m_timeSinceLastSpawn += 1.0f;
		// Decide whether to spawn new enemy
		if ( m_timeSinceLastSpawn >= m_timeBetweenSpawns ) {
			int rand = m_randGenerator.nextInt(10);
			if ( rand == 0 )
			{
				// Choose what kind of enemy
				int type = m_randGenerator.nextInt(6);
				int behavior = m_randGenerator.nextInt(2);
				
				Enemy newEnemy = new Enemy();
				Vector2 randCoord = new Vector2();
				randCoord.x = m_randGenerator.nextInt( (int) (m_screenMaxCoords.x/10) ) + m_screenMaxCoords.x;
				randCoord.y = m_randGenerator.nextInt( (int) (m_screenMaxCoords.y - (m_screenMaxCoords.y/10)) );
				
				newEnemy.setup(type, behavior, randCoord);
				newEnemy.setTarget(m_player);
				
				m_lstEnemies.add(newEnemy);
				m_timeSinceLastSpawn = 0.0f;
			}
		}
		
		// Update current enemies
		ArrayList<Enemy> enemiesToRemove = new ArrayList<Enemy>();
		for (Enemy e : m_lstEnemies) {
			e.handleMovement();
			e.update(delta);
			if ( e.releaseBullet() ) {
				m_lstBullets.add(new Bullet(e.getChargingBullet()));
			}
			
			if ( e.getRight() < 0 ) {
				enemiesToRemove.add(e);
			}
		}
		
		// Remove enemies off the side of the screen
		for (Enemy e : enemiesToRemove) {
			m_lstEnemies.remove(e);
		}		
	}
	
	private boolean isCollision(EntityBase obj1, EntityBase obj2) {
		if ( obj1.getLeft() < obj2.getRight() &&
				obj1.getRight() > obj2.getLeft() &&
				obj1.getTop() > obj2.getBottom() &&
				obj1.getBottom() < obj2.getTop() ) {
			return true;
		}
		return false;
	}
}




