package com.moosader.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

// This is meant to allow sub-screens in the game.
// Screen will be square-shaped, and placed based on the orientation of
// the Android screen.  The game screen will be drawn within the constraints
// of this Screen, while things like the on-screen D-pad are drawn outside.
public class GameScreen {
	private ArrayList<Sprite> m_lstSprites;
	
	//private final int screenWidth = Gdx.graphics.getWidth();
	//private final int screenHeight = Gdx.graphics.getHeight();
	// Square screen assumed
	private final float ratio = Gdx.graphics.getWidth() / 1000f;
	
	public void setup() {
		m_lstSprites = new ArrayList<Sprite>();
	}
	
	public void add(Sprite sprite) {
		// Create new sprite, scale it, and add.
		Sprite scaledSprite = new Sprite(sprite);
		scaledSprite.setSize(sprite.getWidth()*ratio, sprite.getHeight()*ratio);
		// Change coordinates
		scaledSprite.setPosition(sprite.getX()*ratio, sprite.getY()*ratio);
		m_lstSprites.add(scaledSprite);
	}

	public void drawScaledGraphics(SpriteBatch batch) {
		for (Sprite s : m_lstSprites)
		{
			s.draw(batch);
		}
		m_lstSprites.clear();		
	}
}
