package com.moosader.entities;

public enum EnemyType {
	PISTOLAGENT1(1),
	PISTOLAGENT2(2),
	SHOTGUNAGENT1(3),
	SHOTGUNAGENT2(4),
	DUALAGENTS1(5),
	DUALAGENTS2(6);
	
	private int value;
	private EnemyType(int value) {
		this.value = value;
	}
};
