package com.moosader.entities;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.moosader.screens.GameScreen;

public class Character extends EntityBase {
	/*
	public Sprite m_sprite;
	public TextureRegion m_region;
	public Texture m_texture;
	protected Vector2 m_coord;
	protected Vector2 m_dimen;
	 */
	
	protected Bullet m_chargingBullet;
	protected float m_shootCooldown;
	protected float m_shootCooldownMax;
	protected Sprite m_bulletSprite;
	protected ArrayList<Sprite> m_lstHPSprites;
	
	protected float m_frame;
	
	protected Vector2 m_speed;
	protected int m_hp;
	protected int m_totalHP;
	protected float m_scale;
	
	protected void setup() {
		m_lstHPSprites = new ArrayList<Sprite>();
		m_chargingBullet = new Bullet();
	}
		
	public void update(float delta) {
		handleGun();
		move(delta);
		
		if (m_shootCooldown > 0) {
			m_shootCooldown -= 1.0f;
			m_chargingBullet.setInactive();
		}
		
		updateFrame();
	}
	
	public void addToSpriteList(GameScreen screen) {
		screen.add(m_sprite);
		
		if (m_chargingBullet.getState() == BulletState.CHARGING) {
			screen.add(m_chargingBullet.m_sprite);
		}
		
		for(Sprite s : m_lstHPSprites) {
			screen.add(s);
		}
	}
	
	public void move(float delta) {
		
	}
	
	public boolean releaseBullet() {
		return m_chargingBullet.getState() == BulletState.PROJECTILE;
	}
	
	public Bullet getChargingBullet() {
		Bullet releasedBullet = new Bullet(m_chargingBullet);
		m_chargingBullet.setInactive();
		// Don't let player/enemy shoot for a while.
		m_shootCooldown = m_shootCooldownMax;
		return releasedBullet;
	}
	
	protected void handleGun() {
		
	}
	
	protected void handleMovement() {
		
	}
	
	protected void incrementFrame() {
		
	}
	
	protected void updateFrame() {
		incrementFrame();
		m_sprite.setRegion(m_region);
		m_sprite.setPosition(m_coord.x, m_coord.y);
		m_sprite.setSize(m_dimen.x, m_dimen.y);
	}
	
	public void hitByBullet(float bulletSize) {
		m_hp -= bulletSize * 7f;
	}
	
	public int getHP() { 
		return m_hp;
	}
}
